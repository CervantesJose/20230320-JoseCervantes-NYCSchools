//
//  NetworkEndpoints.swift
//  20230320-JoseCervantes-NYCSchools
//
//  Created by Jose Cervantes on 3/20/23.
//

import Foundation

struct NetworkEndpoint<Response: Decodable> {
    let url: URL
    let responseType: Response.Type
    init(url: URL, responseType: Response.Type) {
        self.url = url
        self.responseType = responseType
    }
}

struct NetworkEndpoints {
    //MARK: func score
    static func score(dbn: String) -> NetworkEndpoint<[SAT]> {
        let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)")!
        return NetworkEndpoint(url: url, responseType: [SAT].self)
    }
    //MARK: func schools
    static func schools() -> NetworkEndpoint<[School]> {
        let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!
        return NetworkEndpoint(url: url, responseType: [School].self)
    }
}
