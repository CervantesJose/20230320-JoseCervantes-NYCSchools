//
//  SchoolListView.swift
//  20230320-JoseCervantes-NYCSchools
//
//  Created by Jose Cervantes on 3/20/23.
//

import SwiftUI

struct ListView: View {
    
    @ObservedObject var viewModel: ViewModel
    @State private var scrollOvershoot: CGFloat = 0.0
    
    var body: some View {
        GeometryReader { outerGeometry in
            ScrollView {
                LazyVStack {
                    ForEach(viewModel.schools) { school in
                        Button {
                            viewModel.select(school: school)
                        } label: {
                            schoolCell(school)
                        }
                    }
                }
            }
            .frame(width: outerGeometry.size.width)
        }
        .navigationDestination(for: SchoolAndSAT.self) { schoolAndSAT in
            DetailView(schoolAndSAT: schoolAndSAT, viewModel: viewModel)
        }
        .alert("Schools could not load.", isPresented: $viewModel.failedToFetchSchools) {
            Button("Dismiss") { }
        }
        .alert("SAT scores could not load", isPresented: $viewModel.failedToFetchSATs) {
            Button("Dismiss") { }
        }
        .overlay(LoadingOverlay(isLoading: (viewModel.isFetchingSATs || viewModel.isFetchingSchools)))
    }
    
    func schoolCell(_ school: School) -> some View {
        let name = school.school_name ?? "Unknown"
        return HStack {
            VStack {
                HStack {
                    Text(name)
                        .font(.title)
                        .foregroundColor(Color(.black))
                    
                    Spacer()
                    Image(systemName: "chevron.right")
                        .foregroundColor(Color(red: 0.15, green: 0.55, blue: 0.75))
                        .frame(width: 40, height: 52)
                        .padding(.all, 6)
                }
                .padding(.leading, 16)
                .padding(.vertical, 12)
            }
            .background(RoundedRectangle(cornerRadius: 10).fill().foregroundColor(Color(red: 0.8, green: 0.9, blue: 0.955)))
            .background(RoundedRectangle(cornerRadius: 10).stroke().foregroundColor(Color(red: 0.725, green: 0.725, blue: 0.725)))
            
        }
        .padding(.horizontal, 18)
        .padding(.vertical, 8)
        
    }
}

struct SchoolListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView(viewModel: ViewModel.preview())
    }
}

