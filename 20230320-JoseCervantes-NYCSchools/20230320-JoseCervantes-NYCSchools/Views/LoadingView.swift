//
//  LoadingView.swift
//  20230320-JoseCervantes-NYCSchools
//
//  Created by Jose Cervantes on 3/20/23.
//

import SwiftUI

struct LoadingOverlay: View {
    var isLoading: Bool
    
    @ViewBuilder
    var body: some View {
        if isLoading {
            VStack {
                Spacer()
                HStack {
                    Spacer()
                    ZStack {
                        ProgressView()
                            .progressViewStyle(CircularProgressViewStyle())
                            .dynamicTypeSize(.xLarge)
                            .tint(.blue)
                    }
                    .frame(width: 64, height: 64)
                    .background(RoundedRectangle(cornerRadius: 12).fill().foregroundColor(.white.opacity(0.90)))
                    Spacer()
                }
                Spacer()
            }
            .background(Color.black.opacity(0.85))
        }
    }
}

struct LoadingOverlay_Previews: PreviewProvider {
    static var previews: some View {
        LoadingOverlay(isLoading: true)
    }
}
