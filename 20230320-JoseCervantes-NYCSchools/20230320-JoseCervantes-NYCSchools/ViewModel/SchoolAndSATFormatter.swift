//
//  SchoolAndSATFormatter.swift
//  20230320-JoseCervantes-NYCSchools
//
//  Created by Jose Cervantes on 3/20/23.
//

import Foundation

struct schoolAndSATFormatter {
    
    let schoolAndSAT: SchoolAndSAT
    init(schoolAndSAT: SchoolAndSAT) {
        self.schoolAndSAT = schoolAndSAT
    }
    
    func name() -> String {
        return schoolAndSAT.school.school_name ?? ""
    }
    
    func website() -> String {
        return "https://\(schoolAndSAT.school.website ?? "")"
    }
    
    func addressLine1() -> String {
        return schoolAndSAT.school.primary_address_line_1 ?? ""
    }
    
    func addressLine2() -> String {
        let part1 = schoolAndSAT.school.city ?? ""
        let part2 = schoolAndSAT.school.state_code ?? ""
        let part3 = schoolAndSAT.school.zip ?? ""
        return "\(part1), \(part2) \(part3)"
    }
    
    func numberOfStudents() -> String {
        return "Number of students: \(schoolAndSAT.sat.num_of_sat_test_takers ?? "")"
    }
    
    func criticalReading() -> String {
        return "Critical Reading: \(schoolAndSAT.sat.sat_critical_reading_avg_score ?? "")"
    }
    
    func writing() -> String {
        return "Writing: \(schoolAndSAT.sat.sat_writing_avg_score ?? "")"
    }
    
    func math() -> String {
        return "Math: \(schoolAndSAT.sat.sat_math_avg_score ?? "")"
    }
    
}
