//
//  _0230320_JoseCervantes_NYCSchoolsApp.swift
//  20230320-JoseCervantes-NYCSchools
//
//  Created by Jose Cervantes on 3/20/23.
//

import SwiftUI

@main
struct _0230320_JoseCervantes_NYCSchoolsApp: App {
    @StateObject var viewModel = ViewModel()
    var body: some Scene {
        WindowGroup {
            NavigationStack(path: $viewModel.navigationPath) {
                ListView(viewModel: viewModel)
            }
        }
    }
}
