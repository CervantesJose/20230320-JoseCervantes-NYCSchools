//
//  SAT.swift
//  20230320-JoseCervantes-NYCSchools
//
//  Created by Jose Cervantes on 3/20/23.
//

import Foundation

struct SAT: Decodable {
    
    let dbn: String
    let num_of_sat_test_takers: String?
    let sat_critical_reading_avg_score: String?
    let sat_math_avg_score: String?
    let sat_writing_avg_score: String?
    
}

