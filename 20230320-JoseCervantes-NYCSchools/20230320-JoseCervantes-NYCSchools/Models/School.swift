//
//  Schools.swift
//  20230320-JoseCervantes-NYCSchools
//
//  Created by Jose Cervantes on 3/20/23.
//

import Foundation

struct School: Decodable {
    let dbn: String
    let school_name: String?
    let building_code: String?
    
    let primary_address_line_1: String?
    let city: String?
    let zip: String?
    let state_code: String?
    
    let website: String?
    
}

extension School: Identifiable, Hashable {
    var id: String { dbn }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
