//
//  School+SAT.swift
//  20230320-JoseCervantes-NYCSchools
//
//  Created by Jose Cervantes on 3/20/23.
//

import Foundation

struct SchoolAndSAT {
    let school: School
    let sat: SAT
    init(school: School, sat: SAT) {
        self.school = school
        self.sat = sat
    }
}

extension SchoolAndSAT: Identifiable, Hashable {
    var id: String { school.dbn }
    
    static func == (lhs: SchoolAndSAT, rhs: SchoolAndSAT) -> Bool {
        lhs.school.dbn == rhs.school.dbn
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
