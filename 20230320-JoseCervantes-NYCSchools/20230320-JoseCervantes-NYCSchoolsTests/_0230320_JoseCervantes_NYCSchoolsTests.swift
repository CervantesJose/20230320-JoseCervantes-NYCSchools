//
//  _0230320_JoseCervantes_NYCSchoolsTests.swift
//  20230320-JoseCervantes-NYCSchoolsTests
//
//  Created by Jose Cervantes on 3/20/23.
//

import XCTest
@testable import _0230320_JoseCervantes_NYCSchools

final class _0230320_JoseCervantes_NYCSchoolsTests: XCTestCase {

    func testSchools() {
        let expectation = XCTestExpectation(description: "Download data expectation.")
        let viewModel = ViewModel()
        viewModel.fetchSchools()
        DispatchQueue.global(qos: .background).async {
            Thread.sleep(forTimeInterval: 10)
            if viewModel.schools.count > 0 {
                expectation.fulfill()
            } else {
                XCTFail("No schools found")
            }
        }
        wait(for: [expectation], timeout: 15.0)
    }

}
